from scrapy_djangoitem import DjangoItem
from pccom.models import Pccom

class ScrapperPccomItem(DjangoItem):
    # define the fields for your item here like:
    django_model = Pccom
