import json
import codecs
from pccom.models import Pccom
from scrapy.exceptions import DropItem

class DuplicatesPipeline(object):

    def __init__(self):
        self.names = list()

    def process_item(self, item, spider):
        if item['name'] in self.names:
            raise DropItem("Articulo duplicado: %s" % item)
        else:
            self.names.append(item['name'])
            return item

class DataCleanerPipeline(object):
    
    def process_item(self, item, spider):

        #Check if item exists and updates it or creates it
        Pccom.objects.update_or_create(
            name = str(item['name']).replace("[","").replace("]","").replace("'", ""),
            defaults = {
                'category' : str(item['category']).replace("[","").replace("]","").replace("'", ""),
                'price' : item['price']
            }
        )

        #item['name'] = str(item['name']).replace("[","").replace("]","").replace("'", "")
        #item['category'] = str(item['category']).replace("[","").replace("]","").replace("'", "")

        return item

class DjangoPipeline(object):
    
    def process_item(self, item, spider):
        #item.save()
        return item 
