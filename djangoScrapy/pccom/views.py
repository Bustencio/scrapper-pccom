from django.shortcuts import render
from django.views.generic.list import ListView
from .models import Pccom    
from .forms import CategoryForm

# Create your views here.

def pccom_list(request):
    cats = list()
    items = Pccom.objects.order_by('category')
    for item in items:
        if item.category not in cats:
            cats.append(item.category)

    if request.method == 'POST':
        cat = request.POST.get('cat')
        search = request.POST.get('search')
        if search == "":
            items = Pccom.objects.filter(category=cat)
        else:
            items = Pccom.objects.filter(name__icontains=search)

        return render(request, 'pccom/pccom-list.html', {'items': items, 'cats': cats})
    else:
        return render(request, 'pccom/pccom-list.html', {'items': items, 'cats': cats})


    