from django.core.management.base import BaseCommand
from scrapper_pccom.spiders import pccom
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

class Command(BaseCommand):
    help = 'Crawl/Scrape web'

    def handle(self, *args, **options):
        process = CrawlerProcess(get_project_settings())

        process.crawl(pccom)
        process.start()