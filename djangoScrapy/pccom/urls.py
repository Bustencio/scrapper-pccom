from django.urls import path
from . import views

app_name = 'pccom'
urlpatterns = [
    path('', views.pccom_list, name= 'pccom_list'),
]