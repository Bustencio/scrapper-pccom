from django.db import models

# Create your models here.

class Pccom(models.Model):
    name = models.CharField(max_length=200, unique=True)
    price = models.FloatField()
    category = models.CharField(max_length=50)