from django_elasticsearch_dsl import Document
from elasticsearch_dsl import Index
from .models import Pccom


# The name of your index
item = Index('items')
# See Elasticsearch Indices API reference for available settings
item.settings(
    number_of_shards=1,
    number_of_replicas=0
)


@registry.register_document
@item.document
class PccomDocument(Document):
    class Django:
        # The model associated with this Document
        model = Pccom 
        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'name',
            'price',
            'category',
        ]