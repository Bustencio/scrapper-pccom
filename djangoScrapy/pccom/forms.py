from django import forms

class CategoryForm(forms.Form):
    cat = forms.CharField(max_length=200)