# scrapper-pccom

Proyecto de Django con Scrapy, captura la información de los articulos de una web y los almacena en la base de datos para su posterior uso.

### Setup

- Use python 3.7
- Create virutalenv
- Install requirements

````bash
git clone git@gitlab.com:Bustencio/scrapper-pccom.git
cd scrapper-pccom
mkvirtualenv -p python3
pip install -r requirements.txt
````

### Run

Una vez lanzado el servidor, pagina principal: **http://127.0.0.1:8000/pccom/**

Cargar los datos en la base de datos: **scrapy crawl pccom** 